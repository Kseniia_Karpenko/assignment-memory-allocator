#include <stdio.h>
#include "tests.h"

void conduct_random_test(size_t const test_length, void* heap)
{
    static size_t test_number = 1;
    fprintf(stderr, "**************Start test number №%zu**************\n", test_number);
    debug_heap(stdout, heap);
    uint8_t* test = (uint8_t*)_malloc(test_length);
    debug_heap(stdout, heap);
    fprintf(stderr, "Memory has been allocated\n");
    for(size_t i = 0; i < test_length; i++) {
	test[i] = (uint8_t)(rand() % 255);
    }
    debug_heap(stdout, heap);
    _free(test);
    fprintf(stderr, "The memory was released\n");
    debug_heap(stdout, heap);
    test_number++;
}

void get_more_tests(size_t const counts)
{
    void* heap = heap_init(10000); 
    for(size_t i = 0; i < counts; i++) {
	conduct_random_test(rand() % 3000, heap);
    }
    fprintf(stderr, "Finished %zu tests\n", counts);
}

void first_test()
{
    fprintf(stderr, "Start first basic test\n");
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* temp = _malloc(1000);
    *temp = 525;     
    debug_heap(stdout, heap);
    fprintf(stderr, "First basic test finished\n");
}

void second_test()
{
    fprintf(stderr, "Start second basic test\n");
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* temp1 = _malloc(1000);
    uint16_t* temp2 = _malloc(1500);
    *temp2 = 572;
    *temp1 = 742;
    debug_heap(stdout, heap);
    _free(temp2);
    debug_heap(stdout, heap);
    fprintf(stderr, "Second basic test finished\n");

}

void third_test() {
    fprintf(stderr, "Start third basic test\n");
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* temp1 = _malloc(1000);
    uint16_t* temp2 = _malloc(1500);
    uint16_t* temp3 = _malloc(2000);
    *temp2 = 572;
    *temp1 = 742;
    *temp3 = 732;
    debug_heap(stdout, heap);
    _free(temp2);
    debug_heap(stdout, heap);
    _free(temp1);
    debug_heap(stdout, heap);
    fprintf(stderr, "Third basic test finished\n");
}

void fourth_test()
{
    fprintf(stderr, "Start fourth basic test\n");
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* temp1 = _malloc(65536);
    debug_heap(stdout, heap);
    uint16_t* temp2 = _malloc(254);
    *temp1 = 263;
    debug_heap(stdout, heap);
    *temp2 = 1101;
    debug_heap(stdout, heap);
    fprintf(stderr, "Fourth basic test finished\n");
}

void fifth_test()
{
    fprintf(stderr, "Start fifth basic test\n"); 
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);
    uint16_t* b1 = _malloc(65533 * 2);
    debug_heap(stdout, heap);
    uint16_t* b2 = _malloc(254);
    *b1 = 263;
    debug_heap(stdout, heap);
    *b2 = 1101;
    debug_heap(stdout, heap);
    fprintf(stderr, "Fifth basic test finished\n");
}

void check_basic_tests()
{
    first_test();
    second_test();
    third_test();
    fourth_test();
    fifth_test();
    fprintf(stderr, "Basic tests finished\n");
}
