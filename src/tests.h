#ifndef _TESTS_H_
#define _TESTS_H_

#include <stdio.h>
#include <stdlib.h>
#include "mem.h"
//Запускает 5 основных тестов
void check_basic_tests();

//Запускает тесты с рандомной длинной в количестве counts
void get_more_tests(size_t const counts);

//Проводит рандомный тест с длинной test_length
void conduct_random_test(size_t const test_length, void* heap);

#endif
