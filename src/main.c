#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "tests.h"

int main()
{
    srand(time(NULL));
    check_basic_tests();
    get_more_tests(30);
}
